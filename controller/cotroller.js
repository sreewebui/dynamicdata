
    var app =angular.module("mainapp",["ngRoute"]);
    app.config(function($routeProvider){
       $routeProvider
       .when('/home',{
             templateUrl:"../template/home.html",
             controller:"homeCtrl"
             })
        .when('/dashboard',{
           templateUrl:"../template/dashboard.html",
           controller:"dashCtrl"
       })
        .when('/table',{
           templateUrl:"../template/table.html",
           controller:"tableCtrl"
       })
        .when('/login',{
           templateUrl:"../template/login.html",
           controller:"loginCtrl"
       });
    });
    
    app.controller("homeCtrl",function($scope){
        $scope.message="this is a home controller";
    });
    app.controller("dashCtrl",function($scope){
        $scope.message="this is a dashboard controller";
    });
    app.controller("loginCtrl",function($scope){
        $scope.message="this is a login controller";
    });
    app.controller("tableCtrl",function($scope){
        $scope.message="this is a table controller";
       $scope.data=[];
        $scope.editdata={};
        $scope.submit=function(){
            
            $scope.data.push({name:$scope.name,salary:$scope.salary,location:$scope.location,description:$scope.description});
             $scope.editdata="";
            console.log($scope.data);
        };
        
        $scope.reset=function(){
            $scope.name="";
            $scope.salary="";
            $scope.location="";
            $scope.description=""
        }
        $scope.edit=function(index){
            
           $scope.editdata=$scope.data[index];
            console.log($scope.editdata);
           
               /*{name:$scope.data.name,salary:$scope.data.salary,location:$scope.data.location,description:$scope.data.description};
            console.log($scope.editdata.name);*/
        };
        $scope.delete=function(index){
            $scope.data.splice(index,1);
        }
    });

